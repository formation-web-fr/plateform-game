class Canvas {
    constructor( el ) {
        this.el = el
        this.c = this.el.getContext('2d')
        this.setSize()
    }

    setSize() {
        this.el.width = window.innerWidth
        this.el.height = window.innerHeight
    }

    clear() {
        this.c.clearRect(0, 0, this.el.width, this.el.height)
    }

    name() {
        return this.name
    }
}

export default Canvas