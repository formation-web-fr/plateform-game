import { plateformArray } from './index.js'

class Character {
    constructor(canvas, x, y) {
        this.canvas = canvas
        this.c = canvas.c
        this.x = x
        this.y = y
        this.w = 50
        this.h = 50
        this.jumpSpeed = 25
        this.moveSpeed = 5
        this.velocity = {
            x: 0,
            y: 0
        }
        this.gravity = 1.5
        this.color = 'red'
        this.jumping = false
        this.colisions = {
            bottom: false,
        }
    }

    detectColision() {
        let touchObstacle = false
        let touchFloor = false

        if ( this.y + this.h + this.velocity.y > this.canvas.el.height ) touchFloor = true
        else touchFloor = false

        for ( const item of plateformArray ) {
            if (
                ( this.x + this.w > item.x ) &&
                ( this.y + this.h + this.velocity.y > item.y ) &&
                ( this.x < item.x + item.w ) && 
                ( this.y + this.h < item.y + item.h )
            ) {
                touchObstacle = true
            }
        }

        if (( touchObstacle === true ) || (touchFloor === true)) this.colisions.bottom = true
        else this.colisions.bottom = false
    }

    update() {
        this.x += this.velocity.x
        this.y += this.velocity.y

        this.detectColision()
        
        if ( this.colisions.bottom === false ) this.velocity.y += this.gravity
        else this.velocity.y = 0
        this.draw()
    }

    draw() {
        this.c.fillStyle = this.color
        this.c.fillRect(this.x, this.y, this.w, this.h)
    }
}

export default Character