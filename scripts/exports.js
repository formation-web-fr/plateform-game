function test() {
    console.log('fonction test')
}

function exportSimple() {
    console.log('simple export')
}

function exportSimpledeux() {
    console.log('simple export deux')
}

export default test

export { exportSimple, exportSimpledeux }