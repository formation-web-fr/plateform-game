//------EXEMPLE D IMPORT---------
// import truc, { exportSimple }  from './exports.js'

// console.log('loaded')
// truc()
// exportSimple()

//-----CANVAS-----
import Canvas from './canvas.js'
import Character from './character.js'
import Plateform from './plateform.js'
const canvasEl = document.querySelector('.canvas')
export let plateformArray = []

let scene = new Canvas( canvasEl )
new Plateform(scene, 300, 150, 200)
new Plateform(scene, 600, 200, 200)

// for ( let i = 0; i < 3 ; i ++ ) {
//     let plateform = new Plateform(scene.c, Math.random()*500, Math.random()*150)
//     plateformArray.push(plateform)
// }

let hero = new Character( scene, 100, 150 )



//------ ANIMATION ------
function animate() {
    window.requestAnimationFrame(animate)
    scene.clear()
    hero.update()

    for ( const item of plateformArray ) {
        item.update()
    }
}
animate()


//--------- CONTROLES------
window.addEventListener('keydown', function({keyCode}) {
    switch (keyCode) {
        case 90:
            //top
            if ( hero.colisions.bottom === true ) {
                hero.velocity.y -= hero.jumpSpeed
            }
        break;
        case 81:
            //left
            if ( hero.x > 100 )
            hero.velocity.x = -hero.moveSpeed
            else {
                hero.velocity.x = 0
                for (const item of plateformArray) {
                    item.velocity.x = 5
                }
            }
        break;
        case 68:
            //right
            if ( hero.x < 400 )
            hero.velocity.x = hero.moveSpeed
            else {
                hero.velocity.x = 0
                for (const item of plateformArray) {
                    item.velocity.x = -5
                } 
            }
        break;
    }
})

window.addEventListener('keyup', function({keyCode}) {
    switch (keyCode) {
        case 90:
            //top
        break;
        case 81:
            //left
            hero.velocity.x = 0

            for (const item of plateformArray) {
                item.velocity.x = 0
            }
        break;
        case 68:
            //right
            hero.velocity.x = 0

            for (const item of plateformArray) {
                item.velocity.x = 0
            }
    }
})

//--EVENEMENT--
window.addEventListener('resize', function() {
    scene.setSize()
})

