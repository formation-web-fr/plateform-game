import { plateformArray } from './index.js'

class Plateform {
    constructor(scene, x, y, w, h) {
        this.c = scene.c
        this.x = x
        this.y = scene.el.height - y
        this.w = w
        this.h = 20
        this.velocity = {
            x : 0,
        }
        this.color= "blue"
        plateformArray.push(this)
    }

    update() {
        this.x += this.velocity.x
        this.draw()
    }

    draw() {
        this.c.fillStyle = this.color
        this.c.fillRect(this.x, this.y, this.w, this.h)
    }
}

export default Plateform